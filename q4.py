import random

import requests
from bs4 import BeautifulSoup
import sqlite3
import time

conn = sqlite3.connect('cottages.sqlite')
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS cottages (
description VARCHAR(100),specs VARCHAR(100) ,prices VARCHAR(50))''')
for num in range(1, 6):
    url = f"https://www.holidayfrancedirect.co.uk/cottages-holidays/index.htm?board=sc&d=France&people=2&prop_type%5B0%5D=cottagegite&page={num}"
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    content = soup.find_all('div', class_="col-content")
    for i in content:
        desc = i.find('h2').text
        word = desc.split(" ")
        desc = desc.replace(word[-1], "")
        desc = desc.replace(word[-2], '')
        money = i.find('div', class_='property-pricing').text
        specs = i.find('p', class_='property-spec').text
        mon = money
        m = money.split(" ")
        money = money.replace(f" {m[-1]}", "")
        cur.execute('INSERT INTO cottages (description,specs,prices) VALUES(?,?,?)', (desc, specs, money))
        conn.commit()
    time.sleep(random.randint(15, 20))

cur.close()
