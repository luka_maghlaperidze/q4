# README #



### **რეპოზიტორი** ###

* q4 მოდულში დაწერილ პროგრამას მოაქვს საიტიდან კოტეჯებზე ინფორმაცია და ინახავს მონაცემთა ბაზაში
* V 1.0
* [გამოყენებული ვებგვერდი](https://www.holidayfrancedirect.co.uk/cottages-holidays/index.htm?board=sc&d=France&people=2&prop_type%5B0%5D=cottagegite)

### **ზოგადი ინფორმაცია** ###

* გამოყენებულია **requests**, **bs4**, **sqlite3** მოდულები
BeautifulSoup-ის გამოყენებით get request-ით მიღებულ html სკრიპტს ვამუშავებ და მონაცემთა ბაზაში ვამატებ.
```bash
r = requests.get(url)
soup = BeautifulSoup(r.text, 'html.parser')
```

* იმისათვის რომ სხვა გვერდებიდანაც წამოვიღო ინფორმაცია URL ში page პარამეტრს ციკლის საშუალებით 15დან 20 წამის განმავლობაში გადავცემ მთელ რიცხვებს და ახლიდან ვგზავნი get requestს
```bash
for num in range(1, 6):
url = f'''https://www.holidayfrancedirect.co.uk/
cottages-holidays/index.htm?board=sc&d=France&people=2&prop_type%5B0%5D=cottagegite&page={num}'''
r = requests.get(url)
```



